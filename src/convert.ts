//Les déclarations
import { base, deviseConvert } from "./function";
import { DevicePrompt } from "./function";
const prom = require("prompt-sync")();

let number = "null";
let deDev = "null";
let fiDev = "null";

//Les test (savoir si les prompts sont valide)
while (isNaN(parseFloat(number))) {
  number = prom("Entrez le montant à convertir : ");
}
try {
  deDev = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", base);
} catch (e) {
  console.log(e);
  deDev = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", base);
}
try {
  fiDev = DevicePrompt("Quel est la devise de fin (EUR, CAD, JPY) : ", base);
} catch (e) {
  console.log(e);
  fiDev = DevicePrompt("Quel est la devise de fin (EUR, CAD, JPY) : ", base);
}
console.log(
  "\n" +
    number +
    " " +
    deDev +
    " = " +
    deviseConvert(
      parseFloat(number),
      deDev.toUpperCase(),
      fiDev.toUpperCase()
    ) +
    " " +
    fiDev
);
