const prom = require("prompt-sync")();
export let base = {
    EUR: 1,
    CAD: 1.5,
    JPY: 130,
  };
export function deviseConvert(montant: number, deviseDebut: string, deviseFin: string) {

  //ici on refait des test (on sait jamais)
  if (!Object.keys(base).includes(deviseDebut)) {
    throw new Error("Utiliser les bonne notations pour les devises");
  } else if (!Object.keys(base).includes(deviseFin)) {
    throw new Error("Utiliser les bonne notations pour les devises");
  }
  //Et on renvoie la conversion
  else {
    return montant * (1 / base[deviseDebut]) * base[deviseFin];
  }
}
export function DevicePrompt(message, obj) {
  let prompt = prom(message);
  if (Object.keys(obj).includes(prompt.toUpperCase())) {
    return prompt;
  } else {
    throw new Error("Utiliser les bonne notations pour les devises");
  }
}
export function renseignementExpedition() {
  let poids = "null";
  let long = "null";
  let larg = "null";
  let haut = "null";
  let dest = "null";

  while (isNaN(parseFloat(poids)) || parseFloat(poids) < 0) {
    poids = prom("Entrez le poid du colis (en kg) : ");
  }
  while (isNaN(parseFloat(long)) || parseFloat(long) < 0) {
    long = prom("Entrez la longueur du colis (en cm) : ");
  }
  while (isNaN(parseFloat(larg)) || parseFloat(larg) < 0) {
    larg = prom("Entrez la largeur du colis (en cm) : ");
  }
  while (isNaN(parseFloat(haut)) || parseFloat(haut) < 0) {
    haut = prom("Entrez la hauteur du colis (en cm) : ");
  }
  try {
    dest = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", base);
  } catch (e) {
    console.log(e);
    dest = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", base);
  }
  return {poids:parseFloat(poids),longueur:parseFloat(long),largeur:parseFloat(larg),hauteur:parseFloat(haut),destination:dest.toUpperCase()}
}

export function fraisLivraison(obj) {
  let quoef = 1;
  let supp = 0;
  let baseFrais = {
    EUR: 10,
    CAD: 15,
    JPY: 1000,
  };
  //on verifie le poid du colis et on y attribut un quoef de frais
  switch (true) {
    case obj.poids <= 1:
      break;
    case obj.poids > 1 && obj.poids <= 3:
      quoef = 2;
      break;
    case obj.poids > 3:
      quoef = 3;
      break;
  }
  //ici on verifie si la somme des longueur dépasse les 150, si oui on applique un suplément (qui est la moitié des frais minimum)
  if (obj.longueur + obj.largeur + obj.hauteur >= 150) {
    supp = 0.5;
  }
  
  return baseFrais[obj.destination] * quoef + baseFrais[obj.destination] * supp;
}

export function fraisDouane(valeur: number, pays: string) {
  let taxe = 1;
  //si le pays est le canada et que la valeur du colis dépasse cette somme alors on set la taxe a 15%
  if (pays == "CAD" && valeur > 20 / 1.5) {
    taxe = 1.15;
  }
  //si le pays est le japon et que le colis dépasse cette somme alors la taxe est de 10%
  else if (pays == "JPY" && valeur > 5000 / 130) {
    taxe = 1.1;
  }
  return deviseConvert(valeur, "EUR", pays) * taxe;
}
