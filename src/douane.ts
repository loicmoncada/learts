//Les déclarations
const pr = require("prompt-sync")();
import { DevicePrompt, base, fraisDouane } from "./function";

let valeur = "null";
let pays = "null";

//Les test (savoir si les prompts sont valide)
while (isNaN(parseFloat(valeur))) {
  valeur = pr("Entrez la valeur du colis (EUR) : ");
}
try {
  pays = DevicePrompt("Quel est la devise du pays(EUR, CAD, JPY) : ", base);
} catch (e) {
  console.log(e);
  pays = DevicePrompt("Quel est la devise du pays (EUR, CAD, JPY) : ", base);
}

console.log(
  "\nLes frais de douanes sont de " +
    fraisDouane(parseFloat(valeur), pays.toUpperCase()) +
    " " +
    pays
);
