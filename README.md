
## TS learn

En premier lieu il faut avoir installé node js




## Utilisations

#### Convertir des devises 

```http
  node convert.js
```

    Entrez le montant à convertir : [float]
    Quel est la devise de départ (0 = EUR, 1 = CAD, 2=JPY) : [0<=int<=2 ]
    Quel est la devise de départ (0 = EUR, 1 = CAD, 2=JPY) : [0<=int<=2 ]

#### Exemple 
=> node convert.js

=>10

=>1

=>2

output => 866.66666666
#### Obtenir les frais de port

```http
  node expedition.js
```
    Entrez le poid du colis (en kg) : [float]
    Entrez la longueur du colis (en cm) : [float]
    Entrez la largeur du colis (en cm) : [float]
    Entrez la hauteur du colis (en cm) : [float]
    Quel est la destination du colis (0 = France, 1 = Canada, 2 =Japon)        : [0<=int<=2 ]

#### Exemple 
=> node expedition.js

=>5

=>125

=>150

=>50

=>1

output => 52.5
#### Frais de douanes 

```http
  node douane.js
```
    Entrez la valeur du colis : [float]       
    Quel est la destination du colis (0 = France, 1 = Canada, 2 = Japon) : [0<=int<=2 ]

#### Exemple 
=> node douane.js

=>50

=>2

output => 7150 JPY