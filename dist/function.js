"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fraisDouane = exports.fraisLivraison = exports.renseignementExpedition = exports.DevicePrompt = exports.deviseConvert = exports.base = void 0;
var prom = require("prompt-sync")();
exports.base = {
    EUR: 1,
    CAD: 1.5,
    JPY: 130,
};
function deviseConvert(montant, deviseDebut, deviseFin) {
    //ici on refait des test (on sait jamais)
    if (!Object.keys(exports.base).includes(deviseDebut)) {
        throw new Error("Utiliser les bonne notations pour les devises");
    }
    else if (!Object.keys(exports.base).includes(deviseFin)) {
        throw new Error("Utiliser les bonne notations pour les devises");
    }
    //Et on renvoie la conversion
    else {
        return montant * (1 / exports.base[deviseDebut]) * exports.base[deviseFin];
    }
}
exports.deviseConvert = deviseConvert;
function DevicePrompt(message, obj) {
    var prompt = prom(message);
    if (Object.keys(obj).includes(prompt.toUpperCase())) {
        return prompt;
    }
    else {
        throw new Error("Utiliser les bonne notations pour les devises");
    }
}
exports.DevicePrompt = DevicePrompt;
function renseignementExpedition() {
    var poids = "null";
    var long = "null";
    var larg = "null";
    var haut = "null";
    var dest = "null";
    while (isNaN(parseFloat(poids)) || parseFloat(poids) < 0) {
        poids = prom("Entrez le poid du colis (en kg) : ");
    }
    while (isNaN(parseFloat(long)) || parseFloat(long) < 0) {
        long = prom("Entrez la longueur du colis (en cm) : ");
    }
    while (isNaN(parseFloat(larg)) || parseFloat(larg) < 0) {
        larg = prom("Entrez la largeur du colis (en cm) : ");
    }
    while (isNaN(parseFloat(haut)) || parseFloat(haut) < 0) {
        haut = prom("Entrez la hauteur du colis (en cm) : ");
    }
    try {
        dest = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", exports.base);
    }
    catch (e) {
        console.log(e);
        dest = DevicePrompt("Quel est la devise de début (EUR, CAD, JPY) : ", exports.base);
    }
    return { poids: parseFloat(poids), longueur: parseFloat(long), largeur: parseFloat(larg), hauteur: parseFloat(haut), destination: dest.toUpperCase() };
}
exports.renseignementExpedition = renseignementExpedition;
function fraisLivraison(obj) {
    var quoef = 1;
    var supp = 0;
    var baseFrais = {
        EUR: 10,
        CAD: 15,
        JPY: 1000,
    };
    //on verifie le poid du colis et on y attribut un quoef de frais
    switch (true) {
        case obj.poids <= 1:
            break;
        case obj.poids > 1 && obj.poids <= 3:
            quoef = 2;
            break;
        case obj.poids > 3:
            quoef = 3;
            break;
    }
    //ici on verifie si la somme des longueur dépasse les 150, si oui on applique un suplément (qui est la moitié des frais minimum)
    if (obj.longueur + obj.largeur + obj.hauteur >= 150) {
        supp = 0.5;
    }
    return baseFrais[obj.destination] * quoef + baseFrais[obj.destination] * supp;
}
exports.fraisLivraison = fraisLivraison;
function fraisDouane(valeur, pays) {
    var taxe = 1;
    //si le pays est le canada et que la valeur du colis dépasse cette somme alors on set la taxe a 15%
    if (pays == "CAD" && valeur > 20 / 1.5) {
        taxe = 1.15;
    }
    //si le pays est le japon et que le colis dépasse cette somme alors la taxe est de 10%
    else if (pays == "JPY" && valeur > 5000 / 130) {
        taxe = 1.1;
    }
    return deviseConvert(valeur, "EUR", pays) * taxe;
}
exports.fraisDouane = fraisDouane;
