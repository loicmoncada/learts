"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Les déclarations
var pr = require("prompt-sync")();
var function_1 = require("./function");
var valeur = "null";
var pays = "null";
//Les test (savoir si les prompts sont valide)
while (isNaN(parseFloat(valeur))) {
    valeur = pr("Entrez la valeur du colis (EUR) : ");
}
try {
    pays = (0, function_1.DevicePrompt)("Quel est la devise du pays(EUR, CAD, JPY) : ", function_1.base);
}
catch (e) {
    console.log(e);
    pays = (0, function_1.DevicePrompt)("Quel est la devise du pays (EUR, CAD, JPY) : ", function_1.base);
}
console.log("\nLes frais de douanes sont de " +
    (0, function_1.fraisDouane)(parseFloat(valeur), pays.toUpperCase()) +
    " " +
    pays);
