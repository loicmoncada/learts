"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Les déclarations
var pro = require("prompt-sync")();
var function_1 = require("./function");
var data = (0, function_1.renseignementExpedition)();
console.log("\nles frais vers la devise " +
    data.destination.toUpperCase() +
    " sont de " +
    (0, function_1.fraisLivraison)(data));
