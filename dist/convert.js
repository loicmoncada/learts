"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Les déclarations
var function_1 = require("./function");
var function_2 = require("./function");
var prom = require("prompt-sync")();
var number = "null";
var deDev = "null";
var fiDev = "null";
//Les test (savoir si les prompts sont valide)
while (isNaN(parseFloat(number))) {
    number = prom("Entrez le montant à convertir : ");
}
try {
    deDev = (0, function_2.DevicePrompt)("Quel est la devise de début (EUR, CAD, JPY) : ", function_1.base);
}
catch (e) {
    console.log(e);
    deDev = (0, function_2.DevicePrompt)("Quel est la devise de début (EUR, CAD, JPY) : ", function_1.base);
}
try {
    fiDev = (0, function_2.DevicePrompt)("Quel est la devise de fin (EUR, CAD, JPY) : ", function_1.base);
}
catch (e) {
    console.log(e);
    fiDev = (0, function_2.DevicePrompt)("Quel est la devise de fin (EUR, CAD, JPY) : ", function_1.base);
}
console.log("\n" +
    number +
    " " +
    deDev +
    " = " +
    (0, function_1.deviseConvert)(parseFloat(number), deDev.toUpperCase(), fiDev.toUpperCase()) +
    " " +
    fiDev);
